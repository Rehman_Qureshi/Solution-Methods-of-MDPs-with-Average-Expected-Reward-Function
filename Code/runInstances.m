function [] = runInstances(States, Actions, MatrixStructure, StructureValue, LP)
%{
    Instance Creator
    
    This is the main function of the project which creates Probability 
    & Rewards matrices and runs these matrices through a Policy Iteration 
    solver and a Linear Programming solver. Both solvers are specifically
    designed to work with long-run average rewards structures. After 
    evaluation of each algorithm, the following are printed to files:
    CPU time, gain, and optimal actions. These files are then moved to 
    a Results subdirectory.
 
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        States = number of states
        Actions = number of actions
        MatrixStructure = type of Transition Probability Matrix: Random,
            Banded, or BlockDiag. STRING INPUT
        StructureValue = Value defining structural length. 
            Sparsity for Random
            Bandwidth for Banded
            BlockSize for BlockDiag
        LP = type of Linear Programming solver desired 
            (default to Gurobi Barrier)
    Evaluation ============================================================
        Saved to directories.
%}

%Set LP solver if not set
if nargin < 5 
    LP = 1; %Default to Gurobi Barrier
end

%% Create & Import Data

switch MatrixStructure
    case 'BlockDiag'
        %Create Block Diagonal matrices and save as .mat files 
        fprintf('Creating Probability(%.0fx%.0fx%.0f) and Rewards(%.0fx%.0f) matrices with BlockSize(%1.4f)...\n', States,States,Actions,States,Actions, StructureValue);
        CreateInstancesBlockDiagonal(States, Actions, StructureValue);
    case 'Random'
        %Create sparse matrices and save as .mat files 
        fprintf('Creating Probability(%.0fx%.0fx%.0f) and Rewards(%.0fx%.0f) matrices with sparsity(%1.4f)...\n', States,States,Actions,States,Actions, StructureValue);
        CreateInstances(States, Actions, StructureValue);
    case 'Banded'
        %Create banded matrices and save as .mat files 
        fprintf('Creating Probability(%.0fx%.0fx%.0f) and Rewards(%.0fx%.0f) matrices with bandwidth(%1.4f)...\n', States,States,Actions,States,Actions, StructureValue);
        CreateInstancesBanded(States, Actions, StructureValue);
end
    
%Load in Probability & Rewards matrices 
R = strcat('R',num2str(States),'_',num2str(Actions),'.mat');
P = strcat('P',num2str(States),'_',num2str(Actions),'.mat');
load((R))
load((P))
P = [];
for i = 1:Actions
    P(:,:,i) = full(FinalP{i});
end

clear FinalP

fprintf('Matrices properly created.\n\n');

%% Implement PIA
fprintf('PIA Started:\n');

%Implement PIA 
[dStar,g,h,cpu_time] = mdp_unichain_policy_iteration(P,R);

%Create text file to save results of PIA
PIAResults = strcat('PIA_',num2str(States),'_',num2str(Actions),'.txt');
PIAid = fopen(PIAResults,'w');
fprintf(PIAid,'Policy Iteration:\n');
fprintf(PIAid,'-----------------\n');

%Print Gain of PIA
fprintf(PIAid,'Gain: %f\n',g);
%Print CPU & State/Action Data
fprintf(PIAid,'\nTotal CPU Time: %8.8fs\n\n', cpu_time);
k = 1;
while k <= length(dStar)
    fprintf(PIAid,'Optimal Action at State(%1.0f): %5.0f\n',k, dStar(k));
    k = k+1;
end

fprintf('PIA finished.\n\n');
fclose(PIAid);

%% Implement LP 
fprintf('LP Started:\n');

%{
    LP Solver Nomenclature:
        Gurobi Barrier = 1
        Gurobi Dual-Simplex = 2
        LinProg Barrier = 3
        LinProg Dual-Simplex = 4
%}
if (LP == 1)
    [objective,Matrix,b,x,cpu_timeLP] = mdp_unichain_LP(P,R);
elseif (LP == 2)
    [objective,Matrix,b,x,cpu_timeLP] = mdp_unichain_LP(P,R,1);
elseif (LP == 3)
    [objective,Matrix,b,x,cpu_timeLP] = mdp_unichain_LP_LinProg(P,R);
elseif (LP == 4)
    [objective,Matrix,b,x,cpu_timeLP] = mdp_unichain_LP_LinProg(P,R,1);
end

%Create text file to save results of LP 
LPResults = strcat('LP_',num2str(States),'_',num2str(Actions),'.txt');
LPid = fopen(LPResults,'w');
fprintf(LPid,'Linear Programming:\n');
fprintf(LPid,'-------------------\n');

%Print Gain of LP Solver
fprintf(LPid,'Gain: %f\n',dot(objective,x));

%interpret solution of LP to determine optimal actions in each state
[dStarLP] = LPOutputInterface(Matrix,x);

%Print CPU & State/Action Data
fprintf(LPid,'\nTotal CPU Time: %8.8fs\n\n', cpu_timeLP);
k = 1;
while k <= length(dStarLP)
    fprintf(LPid,'Optimal Action at State(%1.0f): %5.0f\n',k, dStarLP(k));
    k = k+1;
end
fclose(LPid);
fprintf('LP Finished.\n');

%% Check Optimal Decisions

for d = 1:length(dStar)
    if (dStar(d) ~= dStarLP(d))
        error('Optimal solutions do not match.');
    end
end


%% Clean up Working Directory & Move Files

%Delete ResultsFile.lp if it exists
if (exist('ResultFile.lp')) ~= 0
    delete ResultFile.lp;
end

%Move Files:
switch MatrixStructure
    case 'BlockDiag'
        movefile *.txt BlockDiagResults
        movefile *.mat BlockDiagResults
    case 'Random'
        movefile *.txt RandomResults
        movefile *.mat RandomResults
    case 'Banded'
        movefile *.txt BandedResults
        movefile *.mat BandedResults
end

end