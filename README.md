# Revisiting Linear Programming to Solve Markov Decision Processes with Long-Run Average Reward Functions

This project compares the computational performance of different solution methods for discrete time, infinite-horizon Markov decision processes (MDPs) with average reward structures. 
Different instances of MDPs were developed and tested to demonstrate that commercial linear programming methods (particularly barrier methods) are as fast as (or faster than) policy iteration algorithms at solving MDPs with long-run average reward functions.
Several linear programming algorithms were compared to determine the fastest LP method to test against policy iteration algorithm. The analysis was then conducted over varying state size, action size, sparsity patterns, and levels of sparsity. 
This study utilized transition probability matrices with two different sparsity patterns: matrices with random sparsity and matrices with block-tridiagonal structures.
For both cases, it may be concluded that LP methods are comparable to PIA when solving MDPs with relatively small action spaces and a dense transition probability matrix.
This study was heavily based on an earlier work that considered MDPs with discounted reward functions. 
That paper can be found here: (http://www.sciencedirect.com/science/article/pii/S0360835215002545). 


## Markov Decision Processes (MDP)

A Markov decision process (MDP) has 5 basic components:

* **Decision Epochs:** Decision Epochs determine at which point the decision maker may take an action or observe its current state. 
	This project assumes infinite-horizon, discrete-time decision epochs. 

* **States:** An MDP may occupy a single state at a given decision epoch. The implementation of states in this project are the number of row (i-component) and columns (j-component) of the transition probability matrices.
	The implementation is also captured in the rewards matrix (as the number of rows).

* **Actions:** At each decision epoch, the decision maker takes an action from a list of available actions. This list is based on the non-zero elements the (k-components) of the transition probability matrices.
	The implementation is also captured in the rewards matrix (as the number of columns). 
	
* **Transition Probabilities:** A MDP may transition to a new state given its current state and the action the decision maker selects. 
	This transition probability is captured as the probability of going to a new state given the current state and action pair. This is the corresponding value from our transition probability matrices.  

* **Rewards:** Once a MDP transitions to a state at a new decision epoch, it received a reward based on the state and action pair. This is the corresponding value from our rewards matrix.



## Project Components
  
* **CreateInstances.m**: This function creates transition probability and rewards matrices given the number of states, number of actions, and sparsity level desired. 
	The transition probability matrices created from this function are matrices with random sparsity patterns.

* **CreateInstancesBanded.m**: This function creates transition probability and rewards matrices given the number of states, number of actions, and bandwidth desired. 
	The transition probability matrices created from this function are matrices with banded sparsity patterns.	
	
* **CreateInstancesBlockDiagonal.m**: This function creates transition probability and rewards matrices given the number of states, number of actions, and block size desired. 
	The transition probability matrices created from this function are matrices with block-tridiagonal sparsity patterns.
	
* **mdp_unichain_policy_iteration.m**: This function implements the Unichain Policy Iteration Algorithm on the transition probabilities and rewards matrices to solve for an optimal policy. 
	The gain and bias are calculated at the end of each iteration, but only the final gain and computational time are outputted along with the optimal action in each state.

* **mdp_unichain_LP.m**: This function sets up the transition probabilities and rewards matrices into the Dual-Linear Program described by Puterman and passes it into the MATLAB interface of the Gurobi solver. 
	Once Gurobi finds a solution, it is outputted. The final gain is calculated as well.

* **mdp_unichain_LP_LinProg.m**: This function sets up the transition probabilities and rewards matrices into the Dual-Linear Program described by Puterman and passes it into the 
	LinProg solver from the MATLAB Optimization Toolbox. Once LinProg finds a solution, it is outputted. The final gain is calculated as well. This solver was only used to compare speeds of various LP solvers.

* **LPOutputInterface.m**: This function extracts the optimal policy given the MDP as a LP and solution. This is a trivial step used to confirm the LP and PIA have the same optimal policies as the LP is already solved.
	
* **runInstances.m**: This is the main function to execute testing and comparisons. This function calls a *CreateInstance...* function to generate MDP instances (the transition probabilities and rewards matrices),
	flows the MDP data to the PIA algorithm, flows the MDP data to the LP algorithm, calls *LPOutputInterface* to extract the optimal policy from the LP solution, compares the PIA and LP optimal policies to verify that 
	they match, and saves the solution files to a separate directory.

* **GUI_Launcher.m**: This script launches a graphical user interface in MATLAB to run a single test case by calling the *runInstances* function to create and solve a single MDP. This method proved to be far too slow; hence, 
	a sweep over states or actions was also created.
	
* **sweepInstances.m**: This function was created to automate testing. It runs a sweep over either states or actions and requests a min and max value. 
	Given min and max limits, the *runInstances* function is called and executed repeatedly until all instances between min and max have not been executed.

* **RunSweep.m**: This script was developed as an interface to input user variables for sweeps. This function simply calls the *sweepInstances* function multiple times.
	
* **LPComparisionTest.m**: This script was created to compare the various LP solvers to determine which LP solver should be used to compare against PIA. In the end, a mixture of commercial availability and speed determined 
	that Gurobi's Barrier method was the optimal solver.

* **MDPclassifier.m**: This function classifies each MDP as unichain, recurrent, or multichain based on the transition probabilities matrix as an input.

	
	
## Required Software

* MATLAB 2017 or Newer
* Gurobi Version 7.5.1 or Newer
* MATLAB Optimization Toolbox (LinProg)



## Contributing

* Rehman S. Qureshi



## License

* Both MATLAB and Gurobi licenses are required. Older versions may be used, but performance is not guaranteed in this case.



## Acknowledgments

* Daniel Silva - Auburn University

* Josh Gardner - For donating computational resources to this project 
