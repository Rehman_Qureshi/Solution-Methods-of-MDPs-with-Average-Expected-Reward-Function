function [] = CreateInstancesBanded(States, Actions, Bandwidth)
%{ 
    Banded Creator
    
    Creates Transition Probability & Rewards Matrices with random values
    given the size of the state space, action space, and bandwidth.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        States = number of states
        Actions = number of actions
        Bandwidth = size of the bands that make up the Trans. Prob.
            Matrix
    Evaluation ============================================================
        None. P & R are saved to the current directory.
%}


format long;

%% Check Bandwidth Size

if Bandwidth >= States
    error('Bandwidth is too large.');
elseif mod(Bandwidth,2) == 0
    error('Even Bandwidth not allowed.');
end

%% Generate P-Matrix using Seed

rng(7,'v5uniform');

P1 = 0.0000 + (1.0000 + 0.0000) * rand(States,States,Actions);

%% Remove Extra Non-Zero Terms

for a = 1:Actions
    for i = 1:States
        for j = 1:States
            if (j >= (i - (Bandwidth-1)/2) && j <= (i + (Bandwidth-1)/2))
                %Do nothing. P(j|i,a) is a nonzero term
            else
                P1(i,j,a) = 0;
            end
        end
    end
end

clear a i j

%% Normalize P
P1 = double(P1);
for a = 1:Actions
    for i = 1:States
        if ~isempty(find(P1(i,:,a)~=0,1))
            sum_j(i,a) = sum(P1(i,:,a));
            if (sum_j(i,a) > 0.0000)
                P1(i,:,a) = P1(i,:,a)/sum_j(i,a);
            else 
                P1(i,:,a) = 1.0000 - P1(i,:,a);
            end
            continue;
        else
            new = 0.0000 + (1.0000-0.0000)*rand(1.0000,States);
            P1(i,1:States,a) = new;
            sum_j(i,a)=sum(P1(i,:,a));    
            if (sum_j(i,a) > 0.0000)
                P1(i,:,a) = P1(i,:,a)/sum_j(i,a);
            else 
                P1(i,:,a) = 1.0000 - P1(i,:,a);
            end
        end
    end
end

for a = 1:Actions
    % Converting P to sparse matrix
    FinalP{a} = sparse(P1(:,:,a));
end

%% Save Probability Matrix
eval([strcat('save(''P',num2str(States),'_',num2str(Actions),'.mat'',''FinalP'');')]);

%% Seed & Generate Rewards Matrix

rng(7,'v5uniform');
R = 0 + (100 + 0) * rand(States,Actions);

% Output R matrix as a .mat file
eval([strcat('save(''R',num2str(States),'_',num2str(Actions),'.mat'',''R'');')]);

end
