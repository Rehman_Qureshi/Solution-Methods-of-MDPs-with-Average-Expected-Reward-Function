function [dStar,g,h,cpu_time] = mdp_unichain_policy_iteration(P,R,max_iter)
%{ 
    Unichain Policy Iteration Algorithm
    
    Resolution of MDP with long-run average reward structure.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        P(SxSxA) = transition matrix 
            P could be an array with 3 dimensions or a cell array (1xA), 
            each cell containing a matrix (SxS) possibly sparse
        R(SxA) = reward matrix
             R could be an array with 3 dimensions (SxSxA) or
             a cell array (1xA), each cell containing a sparse matrix (SxS)
             or a 2D array(SxA) possibly sparse 
        max_iter = maximum number of iteration to be done, upper than 0, 
             optional (default 1000)
    Evaluation ============================================================
        dStar(Sx1) = Optimal Stationary Policy
        g = Gain
        h = Bias
        cpu_time = Total computational time needed
%}

cpu_time = cputime;
%% Check Inputs

%Set Maximum Number of iterations if not set
if nargin < 3 
    max_iter = 1000; 
end

%Check Dimensions of Rewards Matrix
if (ndims(R) ~= 2) % R must be (SxA)
    error('Dimensional Error: Rewards Matrix is not SxA.');
    
%Check Dimensions of Transition Probability Matrix
elseif (ndims(P) ~= 3) % P must be (SxSxA)
    error('Dimensional Error: Probability Matrix is not SxSxA.');
else
    %Assign S and A (lengths) from Rewards Matrix
    [S,A] = size(R);    
    [S1,S2,A1] = size(P);
    %Check size of states in rewards matrix with prob. matrix
    if (S ~= S1 || S ~= S2 || A ~= A1)
        error('Dimensional Error: State Sizes of Rewards Matrix and Prob. Matrices do not agree.');
    end
end
clear A1 S1 S2

%% Initialize
n = 0; % Initialize a counter
is_done = false; %Termination condition set to false

d = ones(S,1)*2; % Arbitrarily pick Action 2 for each state as an initial policy

while(is_done == false)
    %% Policy Evaluation
    
    for i = 1:length(d)
        r(i,1) = R(i,d(i)); %Choose rewards vector based on decision rule(d) vector
    end
    
    I = eye(S); %Create SxS Identity Matrix 
    
    for i = 1:length(d)
        p(i,:,1) = P(i,:,d(i)); %Choose Prob. Matrix based on decision rule(d) vector
    end
    
    s0 = 1; %Arbitrary 
    
    Q = I - p; %Create Q Matrix
    Q(:,s0) = 1; %replace s0 column with 1's
    [w] = Q\r; %perform Gaussian Elimination
    
    g = w(s0); %Obtain scalar g from s0-th component of w
    
    h = w; %Obtain vector h from w 
    h(s0) = 0; %Set solution condition: h(s0) = 0
    
    %% Policy Improvement
    
    %For each action,
    for i = 1:A 
        %Calculate the R + P*h vector (matrix in end)
        D(:,i) = R(:,i) + P(:,:,i)*h; 
    end
    
    %For each state,
    for i = 1:length(D) 
        %Choose optimal actions and store in next_d (vector)
        [Value,Next_d(i,1)] = max(D(i,:)); 
    end

    fprintf('iteration: %.0f \t\tGain: %5.10f\n',n,g);
    
    %% Termination Check
    
    %if decision rule does not change:
    if (Next_d == d)
        %TERMINATE
        dStar = d;
        is_done = true;
        
    %if decision rule is same:
    else
        is_done = false;
        d = Next_d;
        n = n+1;
    end
    
    %if maximum iterations reached:
    if (n == max_iter)
        is_done = true;
        fprintf('Maximum iterations reached.\n');
    end
end

cpu_time = cputime - cpu_time; %clock cpu time and output

end