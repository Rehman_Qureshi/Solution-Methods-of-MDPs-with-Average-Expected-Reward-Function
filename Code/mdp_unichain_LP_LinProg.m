function [objective,Matrix,beq,x,cpu_time] = mdp_unichain_LP_LinProg(P,R,Method)
%{ 
    Unichain Linear Programming Algorithm
    
    Resolution of MDP with average reward structure with 
    Unichain LP Algorithm. Utilizes MATLAB's built in LP solver (linprog)
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        P(SxSxA) = transition matrix 
            P could be an array with 3 dimensions or a cell array (1xA), 
            each cell containing a matrix (SxS) possibly sparse
        R(SxA) = reward matrix
             R could be an array with 3 dimensions (SxSxA) or
             a cell array (1xA), each cell containing a sparse matrix (SxS)
             or a 2D array(SxA) possibly sparse 
        Method = Linear Programming algorithm used (default is Barrier{2})

    Evaluation ============================================================
        objective(1xSA) = coefficients in front of decision variables
        Matrix(SxSA) = assembled matrix of constraint equations from each
             state/action combination.
        b(Sx1) = other side of general equation: Mx = b
             Per the general algorithm, all values of b are 0 except for
             the summation of the decision variables (which is 1).
        x(SAx1) = solution to LP optimization representing each
             state-action combinations.
        cpu_time = Total computational time needed
%}

cpu_time = cputime; %initialize cpu time

%% Check Inputs

%Set Method Type if not set
if nargin < 3 
    %Default to Barrier Method
    Method = 2; 
end

%Check Dimensions of Rewards matrix
if (ndims(R) ~= 2) % R must be (SxA)
    error('Dimensional Error: Rewards Matrix is not SxA.\n');
    
%Check Dimensions of Probability matrix
elseif (ndims(P) ~= 3) % P must be (SxSxA)
    error('Dimensional Error: Probability Matrix is not SxSxA.\n');
else
    %Assign S and A (lengths) from Rewards matrix
    [S,A] = size(R);    
    [S1,S2,A1] = size(P);
    %Check size of states in rewards matrix with prob. matrix
    if (S ~= S1 || S ~= S2 || A ~= A1) 
        error('Dimensional Error: State Sizes of Rewards Matrix and Prob. Matrices do not agree.');
    end
end
clear A1 S1 S2
%% Create Objective Function

objective = []; %Blank objective function initially
%For each State,
for s = 1:S
    %And each action possible from that state,
    for a = 1:A
        % Calculate the objective function as: r(s,a)*x(s,a)
        objective = [objective, R(s,a)]; %append to rear 
    end
end

%Check objective function length
if length(objective) ~= (S*A)
    error('Objective Function: Array Error in Objective Function');
end

clear s a
%% Create Matrix of Constraints
% Two part matrices creation. 

% Part 1: Calculate values and save them by groups of ACTIONS
M = {};
%For each action,
for a=1:A
%   Calculate the Constraint value: (1 - P(s,a)) or (0 - P(s,a))
    M{a} = (speye(S)-P(:,:,a))';
end

% Part 2: Rearrange matrix to be grouped by state-action combinations
Matrix = [];
% For each State,
for i = 1:length(M{1})
    % For each Action,
    for k = 1:length(M)
        % Obtain the State-Action value
        Matrix = [Matrix, M{k}(:,i)]; %append matrix
    end
end
clear M a i k
%% Create RHS B-Vector 

% Create vector of 0s for b vector
beq = zeros(S,1);

%% Add Final Constraint

%Add a row of ones to the end of the matrix
Matrix(end+1,:) = 1;

%Add a 1 to the b vector corresponding to the same index as the Matrix
beq(length(beq)+1) = 1;

%% Solve Linear Program

%Add variables into a model object & set options
clear model
model.f = -objective;
model.Aineq = [];
model.bineq = [];
model.Aeq = Matrix;
model.beq = beq;
model.lb = zeros(1,length(Matrix));
model.ub = [];
model.solver = 'linprog';

%Method Nomenclature
%{ 
   Dual-Simplex = 1
    Barrier = 2
%}
if (Method == 1) %Dual-Simplex
    model.options = optimoptions('linprog','algorithm','dual-simplex', 'TolCon', 1e-8, 'TolFun', 1e-8);
elseif (Method == 2) %Barrier
    model.options = optimoptions('linprog','algorithm','interior-point', 'TolCon', 1e-8, 'TolFun', 1e-8);
else
    error('LP Solution Method not recognized.');
end

%Solve LP using linprog
x = linprog(model);

cpu_time = cputime - cpu_time; %clock cpu time and output
end
