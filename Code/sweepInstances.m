function [] = sweepInstances(MatrixStruct, StructValue, sweepChoice, low, high, Param)
%{ 
    MDP Sweep
    
    Sweeps through many instances of state or actions of MDP with 
    long-run average reward structure.
    
    ARGUEMENTS ============================================================

        MatrixStruct = type of Transition Probability Matrix: Random,
            Banded, or BlockDiag. STRING INPUT
        StructValue = Value defining structural length. 
            Sparsity for Random
            Bandwidth for Banded
            BlockSize for BlockDiag
        sweepChoice = choice of sweep. May either be states or actions
        low = value set as the lower limit of the sweep variable (state or
            action)
        low = value set as the upper limit of the sweep variable (state or
            action)
        Param = constant value of variable that does not change 
            (opposite of sweepChoice)
    Evaluation ============================================================
        None. Directory contains outputs.
%}

    switch sweepChoice
%% State Sweep
        case 'States'
            %State Sweep: Param=Actions
            while(low <= high)
                runInstances(low, Param, MatrixStruct, StructValue);
                low = low + 1000;
                clc;
            end

%% Action Sweep
        case 'Actions'
            %Action Sweep: Param=States
            while(low <= high)
                runInstances(Param, low, MatrixStruct, StructValue);
                if low < 10
                    low = low + 2;
                elseif (low < 100) && (low >= 10)
                    low = low + 10;
                elseif low >= 100
                    low = low + 100;
                end
                clc;
            end
    end
end