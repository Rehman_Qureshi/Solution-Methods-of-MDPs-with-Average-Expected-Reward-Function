function [] = CreateInstancesBlockDiagonal(States, Actions, BlockSize)
%{ 
    Block-Tridiagonal Creator
    
    Creates Transition Probability & Rewards Matrices with random values
    given the size of the state space, action space, and block size.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        States = number of states
        Actions = number of actions
        BlockSize = size of the NxN blocks that make up the Trans. Prob.
            Matrix
    Evaluation ============================================================
        None. P & R are saved to the current directory.
%}

format long;

%% Check BlockSize and State Space Consistency
blocks = States/BlockSize;
if (mod(States,BlockSize) ~= 0)
    error('Matrix Size Error: State Space or Block Size is not consistent');
end

%% Generate P-Matrices Using Seed
rng(7,'v5uniform');

for Part = 1:3 %Tridiagonal Portion
%% Generate Each Block Matrix
for i = 1:blocks
    P{i} = 0.0000 + (1.0000 + 0.0000) * rand(BlockSize,BlockSize,Actions);
    P{i} = full(P{i});
end

%% Normalize Each Row

for b = 1:blocks %for each block
    for k = 1:Actions %for each action
        for i = 1:BlockSize %for each state
            if ~isempty(find(P{b}(i,:,k)~=0,1))
                sum_j(i,k,b) = sum(P{b}(i,:,k)); %Add all components 
                if (sum_j(i,k) > 0.0000)
                    P{b}(i,:,k) = P{b}(i,:,k)/sum_j(i,k,b); %divide by sum
                else 
                    P{b}(i,:,k) = 1.0000 - P{b}(i,:,k);
                end
                continue;
            else
                new = 0.0000 + (1.0000-0.0000)*rand(1.0000,BlockSize);
                P{b}(i,1:BlockSize,k) = new;
                sum_j(i,k,b)=sum(P{b}(i,:,k));    
                if (sum_j(i,k) > 0.0000)
                    P{b}(i,:,k) = P{b}(i,:,k)/sum_j(i,k,b);
                else 
                    P{b}(i,:,k) = 1.0000 - P{b}(i,:,k);
                end
            end
        end
    end
end

clear b sum_j
%% Create the Block-Diagonal Matrix

% Concatenate Block Calls
str = '';
for i = 1:blocks
   str = strcat(str,'P{',num2str(i),'}(:,:,i)');
   if i ~= blocks
       str = strcat(str,',');
   end
end

%Concatenate Block Diagonal Call
string = strcat('Prob(:,:,i) = blkdiag(',str,');');

%For each action, evaluate BlockDiag Call
for i = 1:Actions
    eval(string);    
end

FinalP{Part} = (Prob(:,:,:));

end
clear str string i k Prob Part P

%% Combine Block Diagonals into Block Tridiagonal
P1 = FinalP{1}; %BlockDiag 1
P2 = FinalP{2}; %BlockDiag 2
P3 = FinalP{3}; %BlockDiag 3

%Create a set of SxBlockSizexA zeros for concatenation
S = zeros(length(P1),BlockSize);
for a = 1:Actions %for each action, 
    Q(:,:,a) = [S, P2(:,:,a)]; %Add extra zeros
    R(:,:,a) = [P3(:,:,a),S]; %Add extra zeros
end
clear P2 P3

Q = Q(:,1:(length(Q)-BlockSize),:); %Shift over ->
R = R(:,(BlockSize+1):(length(R)),:); %Shift over <-
P = P1; %No shift

%Concatenate into final P Matrix
Final(:,:,:) = P(:,:,:) + R(:,:,:) + Q(:,:,:);

%% Last Pass Normalization

%Make sure Ao = Aend
for a = 1:Actions
    for i = BlockSize:-1:1
        for j = BlockSize:-1:1
            Final(end-BlockSize+i, end-BlockSize+j,a) = Final(i,j,a);
        end
    end
end

for a = 1:Actions
    for i = 1:States
        if (i <= BlockSize) || ((States - i) <= BlockSize) %Sum = 2
            sum_j(i,a) = sum(Final(i,:,a));
            if sum_j(i,a) > 0.0000
                Final(i,:,a) = Final(i,:,a)/sum_j(i,a);
            else
                error('Zero Row Error');
            end
        else %sum of 3
            sum_j(i,a) = sum(Final(i,:,a));
            if sum_j(i,a) > 0.0000
                Final(i,:,a) = Final(i,:,a)/sum_j(i,a);
            else
                error('Zero Row Error');
            end
        end
    end
end

for a = 1:Actions
    % Converting P to sparse matrix
    FinalP{a} = sparse(Final(:,:,a));
end

%% Save Probability Matrix
eval([strcat('save(''P',num2str(States),'_',num2str(Actions),'.mat'',''FinalP'');')]);

%% Seed & Generate Rewards Matrix

rng(7,'v5uniform');
R = 0 + (100 + 0) * rand(States,Actions);

% Output R matrix as a .mat file
eval([strcat('save(''R',num2str(States),'_',num2str(Actions),'.mat'',''R'');')]);

end