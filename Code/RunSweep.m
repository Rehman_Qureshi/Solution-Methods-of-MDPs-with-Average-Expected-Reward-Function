%RunSweeps Script
%Rehman Qureshi
%Feb. 9th, 2018

clear all
clc

%% Check for Single Computational Thread Activity

if(maxNumCompThreads ~= 1)
    error('Please start Matlab with the following flag: -singleCompThread');
end

%% Random Experiment

% % Sparsity of 0.01
% sweepInstances('Random', 0.01, 'States', 1000, 10000, 10) % Random State Exp
% movefile RandomResults RandomStateExp01
% sweepInstances('Random', 0.01, 'Actions', 2, 200, 2000) % Random Action Exp
% movefile RandomResults RandomActionExp01

% Sparsity of 0.001
sweepInstances('Random', 0.001, 'States', 1000, 10000, 10) % Random State Exp
movefile RandomResults RandomStateExp001
sweepInstances('Random', 0.001, 'Actions', 10, 60, 2000) % Random Action Exp
movefile RandomResults RandomActionExp001

%% Banded Experiment

%Bandwidth of 5
sweepInstances('Banded', 5, 'States', 3000, 20000, 2) % Banded State Exp
movefile BandedResults BandedStateExp5
sweepInstances('Banded', 5, 'Actions', 2, 500, 3000) % Banded Action Exp
movefile BandedResults BandedActionExp5

%Bandwidth of 7
sweepInstances('Banded', 7, 'States', 3000, 20000, 2) % Banded State Exp
movefile BandedResults BandedStateExp7
sweepInstances('Banded', 7, 'Actions', 2, 500, 3000) % Banded Action Exp
movefile BandedResults BandedActionExp7

%Bandwidth of 9
sweepInstances('Banded', 9, 'States', 3000, 20000, 2) % Banded State Exp
movefile BandedResults BandedStateExp9
sweepInstances('Banded', 9, 'Actions', 2, 500, 3000) % Banded Action Exp
movefile BandedResults BandedActionExp9

%% Block Diag Experiment

% %Block Size of 5
% sweepInstances('BlockDiag', 5, 'States', 3000, 20000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateExp5
% sweepInstances('BlockDiag', 5, 'Actions', 2, 500, 3000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionExp5
% 
% %Block Size of 10
% sweepInstances('BlockDiag', 10, 'States', 3000, 20000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateExp10
% sweepInstances('BlockDiag', 10, 'Actions', 2, 500, 3000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionExp10
% 
% %Block Size of 20
% sweepInstances('BlockDiag', 20, 'States', 3000, 20000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateExp20
% sweepInstances('BlockDiag', 20, 'Actions', 2, 500, 3000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionExp20








