function [] = MDPclassifier(P)
%{
    MDP Transition Probability Structure Classifier
    
    Reads in a transition probabilities matrix and 
    classifies the resulting MDP as unichain or multichain.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        P(SxSxA) = transition matrix 
            P could be an array with 3 dimensions or a cell array (1xA), 
            each cell containing a matrix (SxS) possibly sparse
    Evaluation ============================================================
        None: Outputted as a message or error in the Command Window
%}

%% Check Transition Probabilities Matrix

%Convert if given as a cell array
if iscell(P)
    for i = 1:length(P)
        Prob(:,:,i) = full(P{i});        
    end
    P = Prob;
end

%Check Dimensionality of P
if (ndims(P) ~= 3) % P must be (SxSxA)
    error('Dimensional Error: Probability Matrix is not SxSxA.');
end

[S1,S2,A] = size(P);

if (S2 ~= S1)
        error('Dimensional Error: State Sizes of Rewards Matrix and Prob. Matrices do not agree.');
else
    S = S1;
end

clear S1 S2 i Prob
%% Step 0

% Initialize Unchecked & Checked Sets
Unchecked = []; 
Checked = [];

%Initialize Current state
i = 1;

%Add State 1 to Unchecked Set
Unchecked = [i];

%% Step 1

%Verify if Unchecked Set is empty
while ~isempty(Unchecked) 
    
    %New Current State
    i = Unchecked(1);
    
    %Add i to Checked Set
    Checked = [Checked, i];  
    
    %Remove State i from Unchecked Set
    Unchecked = Unchecked(2:end);
    
    %For each j such that Pij > 0
    for j = 1:S
        %If Pij > 0 (for any action) AND has not been checked or added to Unchecked yet,
        if (sum(P(i,j,:)) > 0) && (~ismember(j,Checked)) && (~ismember(j,Unchecked))
            
            %Add j to Unchecked Set
            Unchecked = [Unchecked, j];
        end
    end 
end

%% Step 2

% Unchecked Set should now be empty, check again 
% with Classification.

%If Unchecked Set = Empty Set 
%   AND Checked Set = Cardinality of Transition Prob Matrix 
if (isempty(Unchecked) && length(Checked) == S)
    
    %Confirmed Unichain
    fprintf('Transition Probability Matrix with %.0f States & %.0f Actions....',S,A);
    fprintf('Confirmed Unichain.\n');
    
%If Unchecked Set = Empty Set 
%   AND Checked Set != Cardinality of Transition Prob Matrix    
elseif (isempty(Unchecked) && length(Checked) ~= S)
    
    %Confirmed Multichain
    fprintf('Transition Probability Matrix with %.0f States & %.0f Actions....',S,A);
    fprintf('Confirmed Multichain.\n');
    
else
    error('Unchecked Set not empty!');
end



