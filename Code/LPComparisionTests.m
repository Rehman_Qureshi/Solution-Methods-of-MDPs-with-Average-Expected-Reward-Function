% LP Comparison Tests
%% Check for Single Computational Thread Activity

if(maxNumCompThreads ~= 1)
    error('Please start Matlab with the following flag: -singleCompThread');
end

%% SECTION 1 - Gurobi Cases Using Barrier

% %Random Gurobi
% sweepInstances('Random', 0.01, 'States', 1000, 4000, 10) % Random State Exp
% movefile RandomResults RandomStateGurobiB
% sweepInstances('Random', 0.01, 'Actions', 10, 40, 3000) % Random Action Exp
% movefile RandomResults RandomActionGurobiB
% 
% %BlockDiag Gurobi
% sweepInstances('BlockDiag', 10, 'States', 1000, 4000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateGurobiB
% sweepInstances('BlockDiag', 10, 'Actions', 10, 40, 5000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionGurobiB

%% SECTION 2 - Gurobi Cases Using Dual Simplex

% %Random Gurobi
% sweepInstances('Random', 0.01, 'States', 1000, 4000, 10) % Random State Exp
% movefile RandomResults RandomStateGurobiDS
% sweepInstances('Random', 0.01, 'Actions', 10, 40, 3000) % Random Action Exp
% movefile RandomResults RandomActionGurobiDS
% 
% %BlockDiag Gurobi
% sweepInstances('BlockDiag', 10, 'States', 1000, 4000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateGurobiDS
% sweepInstances('BlockDiag', 10, 'Actions', 10, 40, 5000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionGurobiDS

%% SECTION 3 - LinProg Cases Using Dual-Simplex

% %Random LinProg
% sweepInstances('Random', 0.01, 'States', 1000, 4000, 10) % Random State Exp
% movefile RandomResults RandomStateLinProgDS
% sweepInstances('Random', 0.01, 'Actions', 10, 40, 3000) % Random Action Exp
% movefile RandomResults RandomActionLinProgDS
% 
% %BlockDiag LinProg
% sweepInstances('BlockDiag', 10, 'States', 1000, 4000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateLinProgDS
% sweepInstances('BlockDiag', 10, 'Actions', 10, 40, 5000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionLinProgDS

%% SECTION 4 - LinProg Cases Using Barrier

% %Random LinProg
% sweepInstances('Random', 0.01, 'States', 1000, 4000, 10) % Random State Exp
% movefile RandomResults RandomStateLinProgB
% sweepInstances('Random', 0.01, 'Actions', 10, 40, 3000) % Random Action Exp
% movefile RandomResults RandomActionLinProgB
% 
% %BlockDiag LinProg
% sweepInstances('BlockDiag', 10, 'States', 1000, 4000, 5) %BlockDiag State Exp
% movefile BlockDiagResults BlockDiagStateLinProgB
% sweepInstances('BlockDiag', 10, 'Actions', 10, 40, 5000) %BlockDiag Action Exp
% movefile BlockDiagResults BlockDiagActionLinProgB
