function [optimalAction] = LPOutputInterface(Matrix,x)
%{ 
    Optimal Action Selector
    
    Selects optimal action based upon LP solution to the Unichain MDP
    problem.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        Matrix(SxSA) = assembled matrix of constraint equations from each
             state/action combination.
        x(SAx1) = solution to LP optimization representing each
             state-action combinations.
    Evaluation ============================================================
        optimalAction(Sx1) = Optimal Stationary Policy
%}

%% Check Inputs & Create OptimalAction Vector

    [S,AS] = size(Matrix);
    S = S - 1; %Number of States
    A = AS/S; %Number of Actions
    optimalAction = zeros(S,1); %Create optimalAction vector
    
    %% Initialize Counters
    j = 1;
    k = 1;
    
    %% Separate X(s,a) into groups of Actions based off of States
    for i = 1:length(x) 
        store(j,k) = x(i); % store collects the state/action value
        k = k+1;
        if rem(i,A) == 0 % if remainder is 0 then create new set
            j = j+1;
            k = 1;
        end
    end
    
    %% Determine Optimal Action
    
    for i = 1:length(store) % for each state,
        [Maxvalue, Index] = max(store(i,:)); % choose the maximum value and index this
        optimalAction(i) = Index; %the index corresponds to the optimal action
    end
end