%GUI for Project
%Rehman Qureshi
%10/11/2017

%{
    GUI Tester
    
    Functions Called:
        runInstances.m
%}

clear all
close all
clc

%% GUI
%{
    Create a Graphic User interface to select number of states, number of
    actions, sparcity of probability matrix, and banded vs. unbanded
    probability matrix.
%}
prompts = {'Number of States: ','Number of Actions: ','Matrix Type: ','Sparsity/BlockSize/BandSize: ','LP Solver:'};
WindowName = 'Instance Information';
defaultans = {'1000','3','Random','0.01','1'};

%Request user input
answer = inputdlg(prompts,WindowName,[1 50; 1 50; 1 50; 1 50; 1 50],defaultans);

%pull information from user input
States = str2num(answer{1});
Actions = str2num(answer{2});
MatrixStructure = answer{3};
StructureValue = str2num(answer{4});
LP = str2num(answer{5});
clear prompts WindowName defaultans answer options choice

%% Run Instances
%{
    Run the single instance selected by the user.
%}
runInstances(States, Actions, MatrixStructure, StructureValue,LP)

clear all
clc
