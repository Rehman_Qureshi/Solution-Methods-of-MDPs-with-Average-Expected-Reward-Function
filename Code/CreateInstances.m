function [] = CreateInstances(States,Actions,Sparsity)
%{ 
    Random Instances Creator
    
    Creates Transition Probability & Rewards Matrices with random values
    given the size of the state space, action space, and sparsity level.
    
    ARGUEMENTS ============================================================
    Let S = number of states, A = number of actions

        States = number of states
        Actions = number of actions
        Sparsity = sparsity of Transition Prob Matrix (must be <1)
    Evaluation ============================================================
        None. P & R are saved to the current directory.
%}

format long;
%% Constants
B = States;

%% Generate P Using Seed

rng(7,'v5uniform');
P1 = 0.0000 + (1.0000 + 0.0000) * rand(States,B,Actions);

%% Normalising P1

for i=1:States %For each state,
        for k=1:Actions %For each action,
            
            if ~isempty(find(P1(i,1:B,k)<Sparsity,1));
                list1= find(P1(i,1:B,k)<Sparsity);
                list2= find(P1(i,1:B,k)>=Sparsity);
                new1=0.0000;
                P1(i,list2,k)=new1;
                  for kk1=1:length(list1);
                      while P1(i,list1(kk1),k)<Sparsity;
                          new1=0.0000 + (1.0000-0.0000)*rand(1.0000,1.0000);
                          P1(i,list1(kk1),k)= new1;
                      end
                  end
            else
                list2= find(P1(i,1:B,k)>=Sparsity);
                new1=0.0000;
                P1(i,list2,k)=new1;
            end
        end
end

%% Normalize Each Row

for k=1:Actions;
    for i=1:States;
        if ~isempty(find(P1(i,:,k)~=0,1));
           sum_j(i,k)=sum(P1(i,:,k));
            if (sum_j(i,k) > 0.0000)
                P1(i,:,k)=P1(i,:,k)/sum_j(i,k);
            else 
                P1(i,:,k)=1.0000-P1(i,:,k);
            end
           continue;
        else
             new = 0.0000 + (1.0000-0.0000)*rand(1.0000,B);
             P1 (i,1:B,k) = new;
             list= find(P1(i,1:B,k)<Sparsity);
             for kk=1:length(list);
                 while P1(i,list(kk),k)<Sparsity;
                       new1=0.0000 + (1.0000-0.0000)*rand(1.0000,1);
                       P1(i,list(kk),k)=new1;
                 end
             end
             sum_j(i,k)=sum(P1(i,:,k));    
             if (sum_j(i,k) > 0.0000)
                 P1(i,:,k)=P1(i,:,k)/sum_j(i,k);
             else 
                 P1(i,:,k)=1.0000-P1(i,:,k);
             end
        end     
    end
    % Converting P to sparse matrix
    FinalP{k} = sparse(P1(:,:,k));
end

% save ('P7.mat','FinalP');
eval([strcat('save(''P',num2str(States),'_',num2str(Actions),'.mat'',''FinalP'');')]);

%% Seed & Generate Rewards Matrix

rng(7,'v5uniform');
R = 0 + (100 + 0) * rand(States,Actions);

% Output R matrix as a .mat file
eval([strcat('save(''R',num2str(States),'_',num2str(Actions),'.mat'',''R'');')]);



    

